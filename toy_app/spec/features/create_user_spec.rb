require "rails_helper"
require "capybara/rails"

RSpec.feature "user creates user" do
  scenario "they try to create the user" do
    user_name = "Test Person"
    user_email = "test@yale.edu"

    visit root_path
    click_on "New User"
    fill_in "user_name", with: user_name
    fill_in "user_email", with: user_email
    click_on "Create User"
    expect(page).to have_content user_name
    expect(page).to have_content user_email
  end
end
