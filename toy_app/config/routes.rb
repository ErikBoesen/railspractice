Rails.application.routes.draw do
  namespace :admin do
      resources :posts
      resources :users

      root to: "posts#index"
    end
  resources :posts
  resources :users
  root 'users#index'
end
