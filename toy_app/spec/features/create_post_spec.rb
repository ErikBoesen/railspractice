require "rails_helper"
require "capybara/rails"

RSpec.feature "user creates post" do
  scenario "they try to create the post" do
    user_name = "Test Person"
    user_email = "test@yale.edu"

    visit root_path
    click_on "New User"
    fill_in "user_name", with: user_name
    fill_in "user_email", with: user_email
    click_on "Create User"

    post_content = "These are some very cool words, aren't they? Lots of detail; very exciting."
    post_user_id = 1

    visit posts_path
    click_on "New Post"
    fill_in "post_content", with: post_content
    fill_in "post_user_id", with: post_user_id
    click_on "Create Post"
    expect(page).to have_content post_content
  end
end
